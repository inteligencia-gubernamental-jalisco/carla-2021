import argparse

from mgpumodule import CIFARMultiGPUModule
from pytorch_lightning import loggers
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser = CIFARMultiGPUModule.add_model_specific_args(parent_parser=parser)
    parser = Trainer.add_argparse_args(parent_parser=parser)
    args = parser.parse_args()

    checkpoint = ModelCheckpoint(
        monitor='val_acc_epoch', mode='max', verbose=True, filename='model-{epoch:03d}'
    )
    board_logger = loggers.TensorBoardLogger(save_dir=args.metrics_path)

    trainer = Trainer.from_argparse_args(args, callbacks=[checkpoint], logger=[board_logger])
    model = CIFARMultiGPUModule(args)
    trainer.fit(model=model)

    ckpt_path = checkpoint.best_model_path
    print('Best checkpoint path:', ckpt_path)

