![http://carla2021.org/](http://carla2021.org/sites/default/files/inline-images/headcarlanew_20210914.png)

# Introducción a Inteligencia Artificial y Supercómputo (CARLA 2021)

La comunidad emergente de Inteligencia Artificial (IA) en la infraestructura de Supercómputo (SC) es fundamental para lograr avances más rápidos, de mayor impacto y que puedan ser escalables. En este taller daremos una introducción teórica-práctica sobre el uso de infraestructura de supercómputo para  problemas de IA. Especialmente abordaremos temas de paralelización y aceleración de  programas con Python y librerias especializadas.

---

## Mentores

- **Ulises Cortés** es Catedrático e Investigador de la Universidad Politécnica de Cataluña (UPC. Trabaja en varias áreas de Inteligencia Artificial (IA)  incluidas la adquisición de conocimiento y la formación de conceptos en sistemas basados en el conocimiento, así como en aprendizaje automático y en agentes inteligentes autónomos. En los últimos cinco años estudia los aspectos éticos, legales, socioeconómicos y culturales de los usos de la IA.
Desde 2017 el profesor Cortés es el director científico del grupo de Inteligencia Artificial de Alto Rendimiento del Centro de Supercomputación de Barcelona.


- **Ulises Moya**  es Director de IA del gobierno del estado de Jalisco. Trabajó como investigador postdoctoral en el grupo de HPAI (Inteligencia Artificial de Alto Rendimiento) del centro de supercómputo de Barcelona. Su trabajo más reciente se enfoca en los límites del aprendizaje profundo. Ha impartido en conjunto  con Abraham Sánchez, el taller de AI+HPC en RIIAA 2020 y RIIAA 2021.  Sus reconocimientos más importantes son la beca Fulbright-García Robles y el Diploma Juan Manual Lozano por el instituto de física de la UNAM.


- **Abraham Sánchez** es analista de IA del gobierno del estado de Jalisco. Tuvo una estancia de investigación en el grupo de Inteligencia Artificial de Alto Rendimiento del Centro de Supercomputación de Barcelona.  Ha publicado más de 5 trabajos en congresos internacionales y revistas especializadas en aprendizaje profundo.  Ha impartido el taller de AI+HPC en RIIAA 2020 y 2021. 

## Objetivos del taller

- Explorar la sinergia que hay entre la IA y el SC.
- Entender el uso que tiene la infraestructura de SC en aplicaciones de IA.
- Resaltar la importancia de uso del de hardware especializado, principalmente en el área del aprendizaje profundo.
- Explorar algunas herramientas para acelerar y paralelizar código base de Python como [Numba](https://numba.pydata.org/).
- Explorar algunas herramientas/plataformas para acelerar y paralelizar modelos de aprendizaje profundo como [Pytorch](https://pytorch.org/) y [Pytorch Lightning](https://www.pytorchlightning.ai/).



--- 

IMPORTANTE: Se recomienda el uso de [Google Colaboratory](https://colab.research.google.com/notebooks/intro.ipynb#recent=true) ya que facilitará mucho la instalación de librerías, así como también que permite el uso de tarjetas gráficas (GPUs) por un determinado periodo de tiempo. Para poder acceder se requerirá de una cuenta de google.


---

##  ¿Cómo trabajar con Google Colaboratory?

Para crear un archivo de Google Colaboratory sigue los siguientes pasos:

1. Ir al siguiente sitio: https://colab.research.google.com/notebooks/intro.ipynb. Aparece lo siguiente:

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/bd23cca5fbb30165aa1928bad0bae0c1/google_colaboratory_main.PNG)

2. Da clic en Archivo > Bloc de notas nuevo.

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/4b2628a5ab41ef5c409eccf9a2963738/google_colaboratory_new.PNG)

3. Posteriormente aparecerá un rectángulo en el cual podrás agregar sentencias como si escribieras en una termina/línea de comandos.

![](https://gitlab.com/inteligencia-gubernamental-jalisco/riiaa20/uploads/fd97489d339be447a40d705a5d7bae34/google_colaboratory_terminal.PNG)



## ¿Cómo trabajar con ambiente de Anaconda/Miniconda?

Para trabajar con Anaconda o Miniconda (versión reducida) se requiere que este sea instalado en nuestros equipos. Para esto realiza lo siguiente:

1. Ir al siguiente sitio: https://docs.conda.io/en/latest/miniconda.html
2. Seleccionar la descarga para el sistema operativo que se esté usando.
3. Una vez descargado, este debe ser instalado.
4. Crear un ambiente a través de los archivos proporcionados en este proyecto (`environment.yml`). Escribe lo siguiente en la terminal.
```bash
conda env create -f environment.yml
```
5. Para visualizar los ambientes:
```bash
conda env list
```
6. Cambiar al ambiente previamente creado.
```bash
conda activate carla21
```
___

## **Jefatura de Gabinete del Gobierno del Estado de Jalisco**
### **Coordinación General de Innovación Gubernamental**
#### **Dirección de Inteligencia Artificial**
